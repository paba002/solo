/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Button,
  DataTable,
  DataGrid,
  Text,
  DateRanger,
  ButtonPanel,
  ActionPanel,
  Tooltip,
  Card,
  Checkbox,
  DropDown,
  Error,
  Loader,
  TabbedPanel,
  TimePicker,
  Input,
  VirtualizedCheckBoxTree,
} from '@trycake/glaze-ui';
import {
  Column,
  Section,
  Container,
  Separator,
  Spacer,
} from '@trycake/glaze-ui/dist/components/Layout';

const columnHeaders = [
  {
    Header: 'Name',
    maxWidth: 240,
    accessor: 'fullName',
    sortingPriority: 1,
    sorting: true,
  },
  {
    Header: 'Email',
    accessor: 'email',
    maxWidth: 250,
    sortingPriority: 2,
    sorting: true,
  },
  {
    Header: 'Location',
    accessor: 'locationList',
    maxWidth: 350,
    sorting: false,
  },
];

const dataList = [
  {
    key: 1,
    fullName: 'paba',
    email: 'paba@mail.com',
    locationList: 'a,s,d,f',
  },
  {
    key: 2,
    fullName: 'paba',
    email: 'paba@mail.com',
    locationList: 'a,s,d,f',
  },
  {
    key: 3,
    fullName: 'paba',
    email: 'paba@mail.com',
    locationList: 'a,s,d,f',
  },
];

export class HomePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dateRangerVisibility: false,
      actionPanelVisibility: false,
    };
  }

  render() {
    return (
      <div className="App">
        <Container>
          <Section sectionSpacing>
            <Column>
              <Text type="h1" isBottomSpacing>
                Test - Glaze UI Components
              </Text>
              <Spacer customPadding={[14, 0, 26, 0]}>
                <Checkbox
                  id="one"
                  label="Awesome Toggle"
                  onClick={() => null}
                  isChecked
                />
              </Spacer>
              <Separator />
            </Column>
          </Section>
          <Section sectionSpacing>
            <Column>
              <Card hasBorder isRaised>
                <Text type="h1">Card Header</Text>
              </Card>
            </Column>
          </Section>
        </Container>

        <Container>
          <Section sectionSpacing>
            <Column xl={12}>
              <Text type="h1">Data Table</Text>
              <DataTable
                triggerBeforeIndex={1}
                overscanRowCount={5}
                useDynamicRowHeight
                columns={columnHeaders}
                dataList={dataList}
                collapsed={[]}
                stickHeaderToTopOnScroll={false}
              />
            </Column>
          </Section>

          <Section sectionSpacing>
            <Column>
              <Text type="h1">Date Ranger</Text>
              <Spacer customPadding={[14, 0, 26, 0]}>
                <Button
                  id="um-add-new-user"
                  isDisabled={false}
                  type="primary"
                  value="Show date ranger"
                  size="x-large"
                  onClick={() => this.setState({ dateRangerVisibility: true })}
                />
              </Spacer>
              <DateRanger
                isVisible={this.state.dateRangerVisibility}
                startEndTime="3.30PM - 5.30PM"
                startTime="3:00 AM"
                endTime="3:00 AM"
                calendarDateOptions={{
                  weekStartsOn: 1,
                  dayPadding: {
                    hours: 0,
                    minutes: 0,
                  },
                }}
                onCancel={() => this.setState({ dateRangerVisibility: false })}
                onApply={() => this.setState({ dateRangerVisibility: false })}
              />
            </Column>
          </Section>
        </Container>

        <Container>
          <Section sectionSpacing>
            <Column>
              <Spacer customPadding={[14, 0, 26, 0]}>
                <Button
                  id="um-add-new-user"
                  isDisabled={false}
                  type="primary"
                  value="Show action panel"
                  size="x-large"
                  onClick={() => this.setState({ actionPanelVisibility: true })}
                />
              </Spacer>
              <ActionPanel
                isVisible={this.state.actionPanelVisibility}
                contents={
                  <div>
                    <p data-tip data-for="cui-tooltip">
                      Content Area
                    </p>
                    <Tooltip id="cui-tooltip">
                      <div>This is a tooltip</div>
                    </Tooltip>
                  </div>
                }
                header="Action Panel Header"
                footer={
                  <ButtonPanel
                    buttonsList={[
                      {
                        type: 'primary',
                        value: 'Save',
                        onClick: () =>
                          this.setState({ actionPanelVisibility: false }),
                      },
                      {
                        type: 'negative',
                        value: 'Delete',
                        onClick: () =>
                          this.setState({ actionPanelVisibility: false }),
                      },
                    ]}
                  />
                }
              />
            </Column>
          </Section>
        </Container>

        <Container>
          <Section sectionSpacing>
            <DataGrid
              gridId="sample-grid-1"
              layoutType="primary"
              clickableRows
              data={[
                {
                  key: 1,
                  id: '001',
                  name: 'Allan',
                  role: 'Manager',
                },
                {
                  key: 2,
                  id: '002',
                  name: 'John',
                  role: 'Employee',
                },
                {
                  key: 2,
                  id: '003',
                  name: 'David',
                  role: 'Owner',
                },
              ]}
              columns={[
                {
                  Header: 'Role',
                  accessor: 'role',
                  minWidth: 400,
                },
                {
                  Header: 'ID',
                  accessor: 'id',
                  maxWidth: 400,
                  minWidth: 400,
                },
                {
                  Header: 'Name',
                  accessor: 'name',
                  maxWidth: 400,
                  minWidth: 400,
                },
              ]}
              loading={false}
              filterable={false}
            />
          </Section>
        </Container>

        <Container>
          <Section>
            <Column>
              <div style={{ position: 'relative', width: 500, height: 150 }}>
                <div style={{ position: 'relative' }}>
                  <Input type="number" iconType="icotime" />
                  <TimePicker
                    radioName="startTime"
                    hours={3}
                    minutes={30}
                    ampms="AM"
                    isNextDay="*"
                  />
                </div>
              </div>
            </Column>
            <Column>
              <DropDown
                dropId="sample-drop-down-1"
                options={[
                  { label: 'Sandwiches', value: '1', selected: true },
                  { label: 'Burgers', value: '2', selected: true },
                  {
                    label:
                      'This is a Loooo oooooooooooo oooooooooooo oooooooo…',
                    value: '3',
                    selected: true,
                  },
                ]}
              />
            </Column>
          </Section>
        </Container>

        <Container>
          <Section>
            <TabbedPanel
              tabListId="custom-tab-list-id"
              onSelect={() => null}
              tabList={[
                {
                  name: 'Guest Manager',
                  content: (
                    <div>
                      <Loader isVisible />
                    </div>
                  ),
                },
                {
                  name: 'Point of sale',
                  content: (
                    <Error
                      isVisible
                      actionList={[
                        {
                          type: 'primary',
                          size: 'large',
                          value: 'Take me home',
                        },
                      ]}
                    />
                  ),
                },
                { name: 'Cake Connect', content: <div /> },
              ]}
            />
          </Section>
        </Container>

        <Container>
          <Section>
            <div>
              <VirtualizedCheckBoxTree
                nodeList={[
                  {
                    id: 1,
                    key: 'admin.portal.access',
                    name: 'Admin Portal Access',
                  },
                  {
                    id: 2,
                    key: 'cake.reports.access',
                    name: 'CAKE Reports',
                  },
                  {
                    id: 3,
                    key: 'pos.reports.access',
                    name: 'POS Reports',
                  },
                ]}
                checkedKeys={{
                  checked: ['pos.reports.access'],
                  halfChecked: [],
                }}
                onCheck={() => null}
              />
            </div>
          </Section>
        </Container>
      </div>
    );
  }
}

export default connect()(HomePage);
